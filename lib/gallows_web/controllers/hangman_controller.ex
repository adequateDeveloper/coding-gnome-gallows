defmodule GallowsWeb.HangmanController do
  use GallowsWeb, :controller

  # GET
  def new_game(conn, _params) do
    render(conn, "new_game.html")
  end

  # POST
  def create_game(conn, _params) do
    game_pid = Hangman.new_game()
    tally = Hangman.tally(game_pid)

    # in the templates these are accessed as assigns, i.e. @game_pid, @tally
    conn
    |> put_session(:game_pid, game_pid)
    |> render("game_field.html", tally: tally)
  end

  # PUT
  def make_move(conn, params) do
    # look at the params map
    # raise inspect params

    game_pid = get_session(conn, :game_pid)
    guess = params["make_move"]["guess"]
    Hangman.make_move(game_pid, guess)
    tally = Hangman.tally(game_pid)

    # usually the value is left in the edit field by default for user validation purposes.
    # but in this case we want to clear out the display input field.
    # Note - params is an element of the conn collection. For convenience params is split out in the function signature.
    # Note - put_in/2 is a Kernel function.
    put_in(conn.params["make_move"]["guess"], "")
    |> render("game_field.html", tally: tally)
  end
end
