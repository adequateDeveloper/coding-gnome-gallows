defmodule Gallows.Views.Helpers.GameStateHelper do
  @moduledoc false

  # Manages both the color and text rendering of the game state status bar.

  # Views (hangman_view) automatically have access to the built-in helpers.
  # But our own helpers do not. Consequently, we explicitly import the needed built-in helper.
  import Phoenix.HTML, only: [raw: 1]

  @responses %{
    :won => {:success, "You Won!"},
    :lost => {:danger, "You Lost!"},
    :good_guess => {:success, "Good guess!"},
    :bad_guess => {:warning, "Bad guess!"},
    :have_used => {:info, "You already guessed that"}
  }

  def game_state(state) do
    @responses[state]
    |> alert()
  end

  defp alert(nil), do: ""

  defp alert({class, message}) do
    """
    <div class="alert alert-#{class}">
      #{message}
    </div>
    """
    |> raw()
    # raw() effectively puts the output into {:safe, """..."""}. We need Phoenix NOT to escape this string
  end
end
