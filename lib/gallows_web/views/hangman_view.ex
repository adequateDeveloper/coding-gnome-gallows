defmodule GallowsWeb.HangmanView do
  use GallowsWeb, :view

  # By default, a view is created for each controller, and functions defined in that view
  # will be available to all templates for that controller.
  # A view can use import to include the functions in another module (useful for sharing helpers
  # between controllers) and a template can explicitly reference helpers outside its view by
  # using the full module/function name.

  import Gallows.Views.Helpers.GameStateHelper

  # called by gallows image rendering
  def turn(left, target) when target >= left, do: "opacity: 1"

  def turn(_left, _target), do: "opacity: 0.1"

  def game_over?(%{game_state: game_state}) do
    game_state in [:won, :lost]
  end

  def new_game_button(conn) do
    # this is a phx-supplied helper button
    # since this is a form button, the click is POSTing back to hangman_path
    # see: router for the post configuration to :create_game
    # see: mix phx.routes for the list of path helpers
    button("New Game", to: hangman_path(conn, :create_game))
  end
end
