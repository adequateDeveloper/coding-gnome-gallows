https://gigalixir.com/

 email support: help@gigalixir.com

https://gigalixir.com/#/dashboard

https://gigalixir.readthedocs.io/en/latest/

https://gigalixir.readthedocs.io/en/latest/main.html#quick-start

https://gigalixir.readthedocs.io/en/latest/main.html#modifying-existing-app


from gallows project folder:
  $ gigalixir login
  $ gigalixir -h
  $ gigalixir account
  $ gigalixir apps
  $ gigalixir logs -a gallows


Deploying using mix
-------------------
https://gigalixir.readthedocs.io/en/latest/main.html#modifying-existing-app-with-mix

  - do not use a config/prod.secret.exs file

https://gigalixir.readthedocs.io/en/latest/main.html#set-up-deploys

  from gallows project folder (git repo)

    $ gigalixir create -n gallows
    $ gigalixir push gigalixir master
    $ curl https://gallows.gigalixirapp.com/


SemaphoreCI - to - Gigalixir PaaS Deployment Configuration
----------------------------------------------------------
  GIGALIXIR_EMAIL=adequate.developer%40gmail.com
  GIGALIXIR_API_KEY=<password-from-~/.netrc-file>
  GIGALIXIR_APP_NAME=gallows
  git remote add gigalixir https://$GIGALIXIR_EMAIL:$GIGALIXIR_API_KEY@git.gigalixir.com/$GIGALIXIR_APP_NAME.git
  git push -f gigalixir HEAD:refs/heads/master    
