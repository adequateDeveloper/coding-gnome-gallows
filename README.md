# Gallows

### A client web view for the Hangman game

[Following the Dave Thomas Elixir for Programmers course](https://coding-gnome.thinkific.com/courses/elixir-for-programmers)

[![Build Status](https://semaphoreci.com/api/v1/adequatedeveloper/coding-gnome-gallows/branches/master/badge.svg)](https://semaphoreci.com/adequatedeveloper/coding-gnome-gallows)

## Features

* Hangman component pulled from GitHub
* Upgraded to Elixir v1.7 and Phoenix v1.3.4
* Semaphore CI - to - Gigalixir PaaS Deployment
* VS Code IDE debug setup for Phoenix

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000/hangman`](http://localhost:4000/hangman) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
